'use strict';

/**
 * @ngdoc overview
 * @name cpManager
 * @description
 * # coursExoApp
 *
 * Main module of the application.
 */
var app = angular.module('CPManager', ['ngRoute']);

    app.run(function($rootScope, $location, serviceAjaxCall){
        $rootScope.root = {
            isConnected: false
        },

        $rootScope.deconnectionClick=function(){
            serviceAjaxCall.deconnection()
              .then(function(res){
                  $rootScope.root.isConnected = false;
                  console.log("Déco");
                  $location.path("/login");
              })
              .catch(function(error){
                  console.log(error);
              })
        }
    }),

  app.config(function ($routeProvider) {
    $routeProvider
      .when('/login', {
        templateUrl: 'partials/login.html',
        controller: 'LoginController'
      })
      .when('/formulaire/:local/:machine', {
        templateUrl: 'partials/formulaire.html',
        controller: 'FormController'
      })
      .when('/admin/list',{
        templateUrl:'partials/list.html',
        controller:'ListMachineController'
      })
      .when('/qr', {
        templateUrl: 'partials/qrcode.html'
      })
      .when('/admin/bugs',{
        templateUrl:'partials/bugsView.html',
        controller:'BugsViewController'
      })
      .when('/admin/bugs/:nameMachine',{
        templateUrl:'partials/bugsView.html',
        controller:'BugsViewController'
      })
      .otherwise({
        redirectTo: '/login'
      });
  });
