
app.factory("serviceAjaxCall",function($http, $rootScope){
    return{
        login: function(email,passwd){
            console.log("email ",email," psw ",passwd);
            return $http.post("/signin", {"emailAdmin" : email,"passwordAdmin" : passwd});
        },
        // Submit form
        submitForm: function(problem){
            return $http.post('/bug', {"problem": problem});
        },

        submitImage: function(data){
            return $http.post('/bug/image', data ,{headers: {'Content-Type': undefined}});
        },

        getMachines: function(){
            console.log("getMachines");
            return $http.get("/machine");
        },
        exporterPDF: function(machines){
            console.log("service PDF : ",machines);
            return $http.post("/pdf",{"machines" : JSON.stringify(machines)}, {responseType: 'arraybuffer'});
        },
        authentificated: function(){
            console.log("authentificated");
            return $http.post('/auth');
        },
        importIpScan: function(file){
            console.log("import IPScan");
            console.log(file);
            return $http.post('/machine',file, {
                headers :{
                    'Content-Type' : undefined
                }
            });
        },
        deconnection : function(){
            return $http.post('/disco');
        },
        getBugs : function(){
            return $http.get("/bug");
        },
        changeStateBug : function(bug){
            return $http.put("/bug/"+bug.id,{'bug' : bug});
        }
    }
});
