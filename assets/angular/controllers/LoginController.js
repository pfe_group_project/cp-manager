
  app.controller('LoginController',['$scope','$location', '$rootScope','serviceAjaxCall', function($scope,$location, $rootScope, serviceAjaxCall) {

    serviceAjaxCall.authentificated().then(result=>{
        $rootScope.root.isConnected = true;
      console.log("LoginController co -> /admin/list");
      $location.path("/admin/list");
    }).catch(err=>{
        $rootScope.root.isConnected = false;
      console.log(err);
    });

    $scope.emailLogin="";
    $scope.passwordLogin="";

    $scope.erreur=false;
    $scope.message="";
  
    $scope.clickLogin=function(){
    serviceAjaxCall.login($scope.emailLogin,$scope.passwordLogin)
      .then(function(res){
        console.log("Dans success de login",res);
        $rootScope.root.isConnected = true;
        $location.path("/admin/list");
      })
      .catch(err=>{
          $rootScope.root.isConnected = false;
        console.log(err);
        $scope.message=err.data;
        $scope.erreur=true;
      })
    };
  }]);
