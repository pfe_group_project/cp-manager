app.controller('ListMachineController', ['$scope', '$location', '$rootScope','serviceAjaxCall', function ($scope, $location, $rootScope, serviceAjaxCall) {

    $scope.toPrint=null;
    $scope.searchMachine='';
    $scope.sortType='name_machine';
    $scope.sortReverse=false;
    $scope.machines = [];
    $scope.loadMachines = function(){

        serviceAjaxCall.getMachines()
        .then(function(results){
            $rootScope.root.isConnected = true;
            $scope.machines = results.data;
        }).catch(err=>{
            $rootScope.root.isConnected = false;
            console.log("ListMachineController pas co -> /login");
            $location.path("/login");
        });
    };

    $scope.exporterPDF=function(){
        console.log($scope.toPrint);
        serviceAjaxCall.exporterPDF($scope.toPrint)
          .then(function(res){
            let a = document.createElement("a");
            a.setAttribute('style', 'display:none;');
            document.body.appendChild(a);

            var pdfFile = new Blob([ res.data ], {
                type : 'application/pdf'
            });
            var pdfUrl = URL.createObjectURL(pdfFile);
            console.log(pdfUrl);

            a.href = pdfUrl;
            a.download = 'qrcode.pdf';
            a.click();
            //var print = window.open(pdfUrl);
            //print.print();
          })
          .catch(err=>{
            console.log(err);
          })
      };
      $scope.exporterUneMachine=function(machine){
          $scope.toPrint=[machine];
          $scope.exporterPDF();
      }
      $scope.exporterToutesLesMachines=function(){
            $scope.toPrint=$scope.machines;
            $scope.exporterPDF();
      }

      $scope.importClick = function(){
            var fileIpScan  = document.getElementById('ip_scan').files[0];
            var data = new FormData();
            data.append("ipScan",fileIpScan);

            serviceAjaxCall.importIpScan(data)
            .then(function(res){
                console.log("on load les machines");
                $scope.loadMachines();
                console.log("on a loadé les machines");

            })
            .catch(err=>{
                console.log(err);
            })
      }

      $scope.deconnectionClick=function(){
          serviceAjaxCall.deconnection()
            .then(function(res){
                console.log("Déco");
                $location.path("/login");
            })
            .catch(err=>{
                console.log("err deco");
            })
      }
      
      $scope.viewBugsClick = function(){
          $location.path("/admin/bugs");
      }

      $scope.viewBugsMachine = function(name_machine){
        $location.path("/admin/bugs/"+name_machine);
      }
      $scope.loadMachines();

}]).filter('namePlaceFilter', function(){
  return function(items, searchMachine) {
    var filtered = [];

    angular.forEach(items, function(value, key) {
      if(value.name_machine.indexOf(searchMachine)!== -1 || value.place.indexOf(searchMachine)!==-1){ // logic for filtering
        this.push(value);
      }
    }, filtered);

    return filtered;
  }
}); 
