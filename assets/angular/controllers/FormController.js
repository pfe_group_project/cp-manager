app.controller("FormController", function(serviceAjaxCall,$scope, $routeParams){
    $scope.problem = {
        machine: $routeParams.machine,
        local: $routeParams.local,
        email: "",
        description: "",
        photo: undefined
    },

    $scope.success = false;

    $scope.requesting = false;

    $scope.error = {
        isSet: false,
        message: ""
    },


    // fileReader --> image event

    $scope.submit = function(){
        // for error messages
        $scope.error.isSet = false;
        $scope.requesting = true;
        $scope.error.message = "";
        // Retrieve the image from the form
        var selectedFile = document.getElementById('imageInput').files[0];
        var data = new FormData();
        // Add the file in the formData
        data.append('image', selectedFile);

        serviceAjaxCall.submitImage(data)
        .then(function(result){
            $scope.problem.photo = result.data;
            serviceAjaxCall.submitForm($scope.problem)
            .then(function(result){
                Materialize.toast('Le problème a été signalé avec succès.', 5000);
                $scope.success = true;
                $scope.requesting = false;
            })
            .catch(function(error){

                if (error.status === 400){
                    $scope.requesting = false;
                    $scope.error.isSet = true;
                    $scope.error.message = error.data;
                }
                console.log(error);
            })
        })
        .catch(function(error){console.log(error)});
    }
});
