app.controller("BugsViewController", function(serviceAjaxCall,$scope, $routeParams, $rootScope ,$location){
    $scope.bugs=[];
    $scope.message="Bugs des machines de l'IPL";
    $scope.machineSelected=$routeParams.nameMachine;
    $scope.states=[
        {
            id : "signale",
            mess : "Signalé",
            selected : ""
        },
        {
            id : "pris_en_charge",
            mess : "Pris en charge",
            selected : ""
        },
        {
            id : "resolu",
            mess : "Résolu",
            selected : ""
        }
    ];

    $scope.selectedState ="";

    $scope.loadBugs = function(){
        serviceAjaxCall.getBugs()
        .then(function(results){
            $rootScope.root.isConnected = true;
            console.log(results.data);
            $scope.bugs=results.data;
            angular.element(document).ready(function(){
                $('.modal').modal();
            })
            //Charger apres que tt soit chargé
            angular.element(document).ready(function () {
                $('.selects').material_select();
            });
        }).catch(function(error){
            console.log(error);
            $location.path('/login');
        })
    };


    $scope.changeStateBugClick = function(bug,state){
        bug.state=state;
        console.log("changeStateBugClick");
        serviceAjaxCall.changeStateBug(bug)
            .then(function(result){
                console.log("changeStateBugClick",result);
            }).catch(function(error){
                console.log("err",error);
            });
    }

    $scope.viewListMachineClick=function(){
        $location.path("/admin/list");
    };

    $scope.loadBugs();

    $scope.$watch($scope.machineSelected,changeMessage);

    $scope.saveChanges= function(bug){
        var select = '#select' + bug.id;
        bug.state = $(select).val();
        serviceAjaxCall.changeStateBug(bug)
            .then(function(result){
                console.log("Mis a jour",result);
                $('.modal').modal('close');
            })
            .catch(function(error){
                console.log(error)
            });
    }  

    function changeMessage(){
        if($scope.machineSelected!==undefined)
            $scope.message="Bugs de la machine "+$scope.machineSelected;
    }  
});
