const csv = require('node-csv').createParser(';');
const Q = require("q");

// api/services/CsvService.js
module.exports = {
  /**
  * Parse the file passed in parameters and add all the machines included in the database
  *
  * @required {String} csvFilePath
  *   The path of the file
  *
  */
  parseAndAddMachines: function(options, done) {
    //Parse the file and get an array
    csv.parseFile(options.csvFilePath, function(err, data) {
      //console.log(data);
      // Browse the array and add each machine
      var operationQueue=[];
      for (var i = 1, len = data.length; i < len; i++) {
        var promise = Machine.add(data[i], options.place);
        operationQueue.push(promise);
      }
      return Q.allSettled(operationQueue);
    });
  }
};
