var qr = require('qr-image');
var fs = require('fs');

module.exports = {
  generateQrCode: function(machine) {
    // Adresse type : /#!/formulaire/<local>/<nomMachine>
    //qr.image("https://cp-manager.herokuapp.com/#!/formulaire/"+machine.place+"/"+machine.name_machine))
    return new Promise(
      function(resolve,reject){
        var url = sails.config.ressources.url;
        var writer = fs.createWriteStream('api/tmp/'+machine.name_machine+'.png');
        qr.image(url+machine.place+"/"+machine.name_machine)
        .pipe(writer);
        writer.on('finish', () => {

          resolve(machine);
        });

      }
    );
  }
};
