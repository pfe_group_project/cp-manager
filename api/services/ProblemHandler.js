// Handle the insertion of the problem

const emailValidator = require('email-validator');

module.exports = {

    insertProblem: function(problem){
        return new Promise(function(resolve, reject){
            // Check if all the data is there
            if (problem.email === "" || problem.description === "" || problem.local === "" || problem.machine === ""){
                reject({status: 400, message: "La requête est pas valide"});
            }
            if (problem.email === undefined || problem.description === undefined || problem.local === undefined || problem.machine === undefined){
                reject({status: 400, message: "La requête n'est pas valide"});
            }
            if (!emailValidator.validate(problem.email)){
                reject({status:400, message: "L'adresse email est mal formée"});
            }
            // Check if the machine exists
            Machine.findByName(problem.machine,problem.local)
            .then(function(machine){
                if (machine === undefined){
                    reject({status:400, message: "La machine n'existe pas."})
                }
                else {
                    if (problem.photo !== undefined && problem.photo !== ''){
                        problem.photo = "images/bugs/" + problem.photo;
                    }
                    var bug = {mail: problem.email.toLowerCase(), picture: problem.photo, description: problem.description, name_machine: problem.machine, date_bug : new Date(), state : "signale"}
                    
                    Bug.insertBug(bug)
                    .then(function(result){
                        var problemsList = machine.bugs;
                        problemsList.push(result);
                        Machine.updateBugList(problem.machine, problemsList)
                        .then(function(result){
                            resolve(result);
                        })
                        .catch(function(error){
                            console.log(error.message);
                            reject({status:500, message: "Internal serveur error"});
                        })
                    })
                    .catch(function(error){
                        console.log(error.message);
                        reject({status:500, message: "Internal serveur error"});
                    })
                }
            })
            .catch(function(error){
                console.log(error.message);
                reject({status:500, message: "Internal serveur error"});
            })

        });
    }
}

function returnError500(reject){
    reject({status:500, message: "Internal serveur error"});
}
