const _bcrypt = require('bcrypt');

module.exports = {
    connect : function(adminFront){
        return new Promise(function(resolve,reject){
            Administrator.getOneByEmail(adminFront.mail_administrator)
                .then(admin =>{
                    if(comparePasswords(adminFront.password_administrator,admin.password_administrator)){
                        resolve(admin);
                    }else{
                        reject("Le mot de passe et/ou l'email est incorrect !");
                    }
                }).catch(err=>{
                    reject(err);
                });
        })
    },

    isAuthentified : function(req){
        var authUser = null;
        if(req.session.admin !== undefined){
            console.log("authentification via session");
            authUser = JSON.stringify(req.session.admin);
        }
        else if(req.signedCookies.admin !== undefined){
            console.log("authentification via cookie");
            req.session.admin = JSON.parse(req.signedCookies.admin);
            authUser = req.signedCookies.admin;
        }
        return authUser;
    }
}

function comparePasswords(passwordEntered, passwordEncrypted) {
    return _bcrypt.compareSync(passwordEntered, passwordEncrypted);
}
