/**
* BugController
*
* @description :: Server-side logic for managing Bugs
* @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
*/

const path = require('path');

const fs = require('fs');

module.exports = {

	create: function(req, res){
		var problem = req.param('problem');
		ProblemHandler.insertProblem(problem)
		.then(function(result){
			res.ok(result);
		})
		.catch(function(error){
			res.status(error.status).send(error.message);
		})
	},

	uploadImage: function(req, res){
		req.file('image').upload({
			dirname: require('path').resolve(sails.config.appPath, 'assets/images/bugs')
		},function (err, uploadedFiles) {
			if (err) {
				return res.negotiate(err);
			}
			if (uploadedFiles.length === 0) {
				return res.ok();
			}
			console.log(uploadedFiles);
			var splittedURI = path.basename(uploadedFiles[0].fd);
			console.log(splittedURI);
			if (!fs.existsSync(path.resolve('.tmp/public/images'))){
				fs.mkdirSync(path.resolve('.tmp/public/images'));
			}
			if (!fs.existsSync(path.resolve('.tmp/public/images/bugs'))){
				fs.mkdirSync(path.resolve('.tmp/public/images/bugs'));
			}
			// copy image to /.tmp
			fs.createReadStream(path.resolve('assets/images/bugs/' + splittedURI)).pipe(fs.createWriteStream(path.resolve('.tmp/public/images/bugs/'+ splittedURI)));
			return res.ok(splittedURI);
		});
	},

	find: function (req, res) {
		var auth = AdminConnection.isAuthentified(req);
		if (!auth) {
			console.log('PAS AUTH BUG.FIND');
			return res.send(401, "Admin not identified");
		}
		Bug.getAll()
			.then(function (results) {
				return res.ok(JSON.stringify(results));
			})
			.catch(err => {
				return res.negotiate(err);
			});

	},

	update : function(req, res){
		var auth = AdminConnection.isAuthentified(req);
		if (!auth) {
			console.log('PAS AUTH BUG.UPDATE');
			return res.send(401, "Admin not identified");
		}
		console.log(req.param('id'));
		var bug = req.param("bug");
		Bug.updateBug(bug)
			.then(result=>{
				Machine.findByNameOnly(bug.name_machine)
					.then(machine=>{
						for(var i = 0; i < machine.bugs.length; i++){
							if(machine.bugs[i].id===bug.id){
								machine.bugs[i].state=bug.state;
								break;
							}
						}
						Machine.updateBugList(machine.name_machine,machine.bugs)
							.then(machine=>{
								res.ok(result);
							}).catch(error=>{
								console.log(err);
								res.negotiate(error);
							})
					}).catch(err=>{
						console.log(err);
						res.negotiate("Probleme maj machines bug");
					})
			}).catch(err=>{
				console.log(err);
				res.negotiate("Probleme maj bugs bug");
			})
	}
};
