/**
 * AdministratorController
 *
 * @description :: Server-side logic for managing Administrators
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	signIn : function(req, res){
        var adminMail=req.param('emailAdmin');
        var adminPassword=req.param('passwordAdmin');
        
        console.log(adminMail);
        console.log(adminPassword);

        var admin={mail_administrator : adminMail,password_administrator:adminPassword};

        AdminConnection.connect(admin)
            .then(result =>{
                admin.password_administrator="";
                req.session.admin=result;
                res.cookie('admin', JSON.stringify(result), {signed:true,maxAge:1800000});
                res.ok(result);
            }).catch(err =>{
                //res.negotiate(err);
                res.send(401, err);
            })

    },
    
    disco : function(req,res){
        try{
            req.session.destroy();
            res.clearCookie('admin');
            res.ok();
        }catch(err){
            console.log("Err: Disco prob");
            res.negotiate(err);
        }
    },

    isAuth : function(req,res){
        console.log("AdminController isAuth");
        var authUser = AdminConnection.isAuthentified(req);
        if(authUser === null){
            return res.send(401,"Vous n'êtes pas authentifié ");
        }
        return res.ok(authUser);
    }
};

