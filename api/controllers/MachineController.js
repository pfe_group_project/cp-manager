/**
 * MachineController
 *
 * @description :: Server-side logic for managing Machines
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	find: function (req, res) {
		var auth = AdminConnection.isAuthentified(req);
		if (!auth) {
			console.log('PAS AUTH MACHINE.FIND');
			return res.send(401, "Admin not identified");
		}
		Machine.getAll()
			.then(function (results) {
				return res.ok(JSON.stringify(results));
			})
			.catch(err => {
				return res.negotiate(err);
			});

	},
	
	create : function(req,res){
		console.log("create machine");
		var ipScan = req.file('ipScan')
		.upload({dirname : require('path').resolve(sails.config.appPath,'public/ipScanFiles')},function(err,uploadedFile){
			if(err){
				console.log(err);
				return res.negotiate(err);
			}
			else{
				console.log(uploadedFile[0]);
				/*[ UploadedFileMetadata {
					fd: '/Users/jojo/Documents/IPL_Bloc3_Quadri1/Projet de fin d\'étude/cp-manager/public/ipScanFiles/33374c95-9ed0-43ff-890a-4a8b9a88f8ee.txt',
					size: 891,
					type: 'text/plain',
					filename: 'ipscan019.txt',
					status: 'finished',
					field: 'ipScan',
					extra: undefined } ]*/
				if(uploadedFile.length>0){
					var place = uploadedFile[0].filename.substring(6, 9);
					var filePath = uploadedFile[0].fd;
					console.log("place : "+place);
					console.log("filePath : "+filePath);
					addMachines({"place" : place,"filePath" : filePath}).then(function(){
						res.ok(uploadedFile.length+" files uploaded successfully");
					})
					.catch(function(err){
						res.negotiate(err);
					})
				}
				
			}
		});


	}
};

function addMachines (data){
	console.log("addMachines");
	console.log("data addMachine : "+data.place+" "+data.filePath);
	
	var promise1 = Machine.setAllInactive(data.place);
	var promise2 = CsvService.parseAndAddMachines({
		csvFilePath: data.filePath,
		place: data.place
	});
	return Promise.all([promise1,promise2]);
}
