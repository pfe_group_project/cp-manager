/**
 * PdfController
 *
 * @description :: Server-side logic for generating machine's pdf
 *
 */

const _html_pdf = require('html-pdf');

const path = require("path");

const Q = require("q");

const express = require("express");

module.exports = {

    create: function (req, res) {

        var operationQueue = [];
        var machinesArray = JSON.parse(req.param('machines'));

        var html = "<!doctype html>" +
            "<html>" +
            "<head>" +
            "<meta charset=\"UTF-8\" />" +
            "<title>CPManager</title>" +
            "<style type=\"text/css\"> td { border : 1px solid; text-align:center;} th.Nom { border : 1px solid; width: 20%;} th.Message { border : 1px solid;width: 50%;} th.QRCode { border : 1px solid;width:30%;}</style>" +
            "</head>" +
            "<body>" +
            "<h2> Liste des étiquettes des machines </h2>" +
            "<table>" +
            "<thead>" +
            "<tr>" +
            "<th class=\"Nom\">Nom machine</th>" +
            "<th class=\"Message\">Message</th>" +
            "<th class=\"QRCode\">QRCode</th>" +
            "</tr>" +
            "</thead>" +
            "<tbody>";


        var options = { format: 'Letter' };

        generateAllQrCode(machinesArray, operationQueue);
        Q.allSettled(operationQueue).then(function (results) {
            for (var i = 0; i < results.length; i++) {
                if (results[i].state === "fulfilled") {
                    var machine = results[i].value;
                    var imgSrc = 'file://' + __dirname + '/../tmp/' + machine.name_machine + '.png';
                    imgSrc = path.normalize(imgSrc);
                    html += "<tr>" +
                        "<td>" + machine.name_machine + "</td>" +
                        "<td>Veuillez scanner le QRCode en cas de problème, merci</td>" +
                        "<td><img src=" + imgSrc + " alt=\"qrcode\"/></td>" +
                        "</tr>";
                }
            }
            html += "</tbody>" +
                "</table>" +
                "</body>" +
                "</html>";

          //  _html_pdf.create(html, options).toFile('./etiquettesMachines.pdf', function (err, data) {
            //    if (err) return console.log(err);
              //  console.log(data); // { filename: '/app/businesscard.pdf' }
                //var filename = __dirname + '/../../etiquettesMachines.pdf';
                //console.log(filename);
                /*res.attachment(data.filename);
                res.send()
                res.download(path.normalize(data.filename),'test.pdf',function(err){
                    if (err) {
                      // Handle error, but keep in mind the response may be partially-sent
                      // so check res.headersSent
                      console.log("ko");
                    } else {
                      // decrement a download credit, etc.
                      console.log("ok");
                    }
                  });*/
              //    res.ok(data);

            //});
            /*_html_pdf.create(html).toBuffer(function(err, buffer){
              res.ok(buffer);
            });*/

            res.set('Content-type', 'application/pdf');
            _html_pdf.create(html).toStream(function(err, stream){
              stream.pipe(res);
            });

        });



        /*
                machinesArray.forEach(machine => {
                    QrCodeGenerationService.generateQrCode(machine)
                    .then(res => {
                        console.log(res);
                        var imgSrc = 'file://' + __dirname + '/../image/'+machine.name_machine+'.png';
                        imgSrc = path.normalize(imgSrc);
                        console.log("source image  : "+imgSrc);
                        html+="<tr>"+
                                "<td>"+ machine.name_machine+"</td>"+
                                "<td> test 2 Veuillez scanner le QRCode en cas de problème, merci</td>"+
                                "<td><img src=" + imgSrc +" alt=\"qrcode\"/></td>"+
                        "</tr>";
                    });

                });

                html+="</tbody>"+
                        "</table>"+
                        "</body>"+
                        "</html>";

                _html_pdf.create(html, options).toFile('./etiquettesMachines.pdf', function (err, res) {
                    if (err) return console.log(err);
                    console.log(res); // { filename: '/app/businesscard.pdf' }
                });
        */
    }

};

function generateAllQrCode(machinesArray, operationQueue) {
    for (var i = 0; i < machinesArray.length; i++) {
        var machine = machinesArray[i];
        var promise = QrCodeGenerationService.generateQrCode(machine);
        operationQueue.push(promise);
    }
}
