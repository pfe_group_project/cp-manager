/**
 * Administrator.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  schema : true,
  autoPK : false,
  tableName : 'administrators',
  autoCreatedAt:false,
  autoUpdatedAt :false,

  attributes: {
    
    mail_administrator :{
      type:"string"
    },

    password_administrator :{
      type:"string"
    }

  },

  getAll : function(){
    return new Promise(function(resolve,reject){
      Administrator.find().exec(function(err,admins){
          if(err){
              console.log(err.message);
              reject(err);
          }else{
              resolve(admins);
          }
      });
    })
  },

  getOneByEmail : function(email){
    return new Promise(function(resolve,reject){
      Administrator.find( {where : { mail_administrator : email}} ).exec(function(err,admin){
        if(err){
          console.log(err.message);
          reject(err);
        }else{
          if(admin.length==0){
            reject("Le mot de passe et/ou l'email est incorrect !");
          }
          resolve(admin[0]);
        }
      })
    })
  }

};

