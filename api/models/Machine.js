/**
 * Machine.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {


    schema: true,
    autoPK: false,
    tableName: 'machines',
    autoCreatedAt: false,
    autoUpdatedAt: false,

    attributes: {

        name_machine: {
            type: 'string'
        },

        ip_address: {
            type: 'string'
        },

        status: {
            type: 'string'
        },

        place: {
            type: 'string'
        },

        comment: {
            type: 'string'
        },

        qrcode: {
            type: 'string'
        },

        bugs: {
            type: 'array'
        }
    },

    getAll: function () {
        return new Promise(function (resolve, reject) {
            Machine.find({ where: { status: "active" } }).exec(function (err, machines) {
                if (err) {
                    console.log(err.message);
                    reject(err);
                } else {
                    resolve(machines);
                }
            });
        });
    },

    findByName: function(name,placeMachine){
        return new Promise(function(resolve,reject){
            Machine.findOne({where: {name_machine: name, place : placeMachine ,status : "active"}}).exec(function(err, machine){
                if (err){
                    console.log(err.message);
                    reject(err);
                }
                else {
                    resolve(machine)
                }
            })
        })
    },

    findByNameOnly: function(name){
        return new Promise(function(resolve,reject){
            Machine.findOne({where: {name_machine: name, status : "active"}}).exec(function(err, machine){
                if (err){
                    console.log(err.message);
                    reject(err);
                }
                else {
                    resolve(machine)
                }
            })
        })
    },

    updateBugList: function (machineName, newBugs) {
        return new Promise(function (resolve, reject) {
            Machine.update({ name_machine: machineName }, { bugs: newBugs }).exec(function (err, machine) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(machine);
                }
            })
        })
    },

    add: function (machine, place) {
        var machineCreated;
        var name_machine = machine[1];
        var ip = machine[0];

        return new Promise(function (resolve, reject) {
            Machine.findOrCreate({ name_machine: name_machine }, { name_machine: name_machine, ip_address: ip, status: 'active', place: place, bugs: [] }).exec(function (err, machine) {
                if (err) {
                    console.log(err.message);
                    reject(err);
                }
                else {
                    Machine.setActive(machine.name_machine);
                    machineCreated = machine;
                    //console.log(machineCreated);
                    resolve(machineCreated);
                }

            });
        });
    },

    // Set all the machine from a place inactive
    setAllInactive: function (place) {
        return new Promise(function(resolve, reject){
            Machine.update({ place: place }, { status: 'inactive' }).exec(function afterwards(err, updated) {
                if (err){
                    console.log(err.message);
                 reject(500)   
                }
                else {
                    resolve(updated);
                }
            });
        });
        
    },

    // Set active the machine whose name is the parameter
    setActive: function (name_machine) {
        return new Promise(function(resolve, reject){
            Machine.update({ name_machine: name_machine }, { status: 'active' }).exec(function afterwards(err, updated) {
                if (err){
                    console.log(err.message)
                    reject(500)
                }
                else{
                    resolve(updated)
                }
            })
        })
    }
};
