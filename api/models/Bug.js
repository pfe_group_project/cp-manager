/**
* Bug.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
*/
const mongodb=require('mongodb');

module.exports = {

    schema : true,
    autoPK : false,
    tableName : 'bugs',
    autoCreatedAt:false,
    autoUpdatedAt :false,

    attributes: {
        mail : {
            type : "string"
        },
        description :{
            type : "string"
        },
        picture : {
            type : "string"
        },
        name_machine : {
            type : 'string'
        },
        date_bug : {
            type : 'date'
        },
        state : {
            type : 'string'
        }
    },

    getAll : function(){
        return new Promise(function(resolve,reject){
            Bug.find().exec(function(err,bugs){
                if(err){
                    console.log(err.message);
                    reject(err);
                }else{
                    resolve(bugs);
                }
            });
        })
    },

    insertBug: function(newBug){
        return new Promise(function(resolve, reject){
            Bug.create(newBug).exec(function(err,bug){
                if(err){
                    reject(err);
                }
                else{
                    resolve(bug);
                }
            })
        })
    },

    updateBug : function(bugToUpdate){
        return new Promise(function(resolve, reject){
            Bug.update({_id : mongodb.ObjectId(bugToUpdate.id)},{state : bugToUpdate.state}).exec(function(err,bug){
                if(err){
                    console.log("updateBug err",err);
                    reject(err);
                }
                else{
                    resolve(bug);
                }
            });
        })
    }

}
